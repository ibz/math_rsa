\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{math_rsa}
\LoadClass[11pt,a4paper]{article}
\RequirePackage[
top=6em,
bottom=6em,
footskip=2em,
%headheight=2em,
headsep=1em
]{geometry}

%Font settings
\RequirePackage[default]{sourcesanspro}
\RequirePackage[T1]{fontenc}

%Enable Mathsupport
\RequirePackage{mathtools}
%Use normal font for math
\RequirePackage{mathastext}

%Support for German
\RequirePackage[utf8]{inputenc}
\RequirePackage{ngerman}

%European style paragraphs
\setlength{\parskip}{\baselineskip}%
\setlength{\parindent}{0pt}%

%Make line spacing bigger
\linespread{1.1}

%Make whitespace in tables bigger
\renewcommand\arraystretch{1.2} % IH. von 1.5 auf 1.2 geändert.

%Enable colours for tables
\RequirePackage[table]{xcolor}
%\RequirePackage{array} % formula rows in tables
%Needed to rotate graphics and tables
\RequirePackage{rotating}

%Support for images
\RequirePackage{graphicx}

%Enable URL highlighting
\RequirePackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=blue,
}
\urlstyle{same}

%Material theme colours
\definecolor{red}{HTML}{F44336}
\definecolor{pink}{HTML}{E91E63}
\definecolor{purple}{HTML}{9C27B0}
%\definecolor{blue}{HTML}{2196F3}
\definecolor{brown}{HTML}{795548}
\definecolor{cyan}{HTML}{00BCD4}
\definecolor{darkgray}{HTML}{616161}
\definecolor{gray}{HTML}{9E9E9E}
\definecolor{lightgray}{HTML}{E0E0E0}
\definecolor{lime}{HTML}{CDDC39}
\definecolor{olive}{HTML}{827717}
\definecolor{orange}{HTML}{FF9800}
\definecolor{teal}{HTML}{009688}
\definecolor{yellow}{HTML}{FFEB3B}
\definecolor{green}{HTML}{388E3C}

%better font for monospace characters
\RequirePackage{sourcecodepro}

% Listing with a box around it                                                  
\RequirePackage[most]{tcolorbox}                                                
\newtcblisting{sexylisting}[2][]{                                               
  sharp corners,                                                                
  fonttitle=\bfseries,                                                          
  colframe=gray,                                                                
  listing only,                                                                 
  listing options={basicstyle=\ttfamily,},                         
  title=\thetcbcounter #2, #1                                                   
}

% Reference a Bibtext item
\RequirePackage{cite}

%sort the bibliography by appearance
\RequirePackage[numbers,sort&compress]{natbib}

%Number Bibliography and include in ToC                                         
\RequirePackage[nottoc,numbib]{tocbibind}

\renewcommand\bibname{Quellenverzeichnis}
